from django.apps import AppConfig


class VirtualCatalogueConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'virtual_catalogue'
