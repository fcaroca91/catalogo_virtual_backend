from django.db import models

# Create your models here.
class VirualCatalogueImage(models.Model):
    id=models.AutoField(
                    auto_created = True,
                    primary_key = True,
                    serialize = False, 
                    verbose_name ='ID'
                )
    title = models.CharField(max_length=100)
    url = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        #return VirualCatalogueImage  title
        return self.title