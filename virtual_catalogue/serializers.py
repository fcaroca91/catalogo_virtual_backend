from rest_framework import routers,serializers,viewsets
from .models import VirualCatalogueImage
class VirualCatalogueImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VirualCatalogueImage
        fields = ['id', 'title', 'url', 'created_at']