from django.urls import path 
from . import views

# define the urls
urlpatterns = [
    path('images/', views.images),
    path('images/<int:pk>/', views.images_detail),
]